#!/bin/bash

## This secondary script is used to run multiple commands in the cloudron user context from start.sh

## Set script to stop if something errors
set -eu

cd /app/data/wappler_app

## Install / Update dependencies, then start the site:
npm install
npm start