FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

## Build folder structure for Cloudron Docker
## code, data, and pkg are all cloudron "system" folders.
## Data is where editable data should go (example: .env config files) This is a read-write folder.
RUN mkdir -p /app/code /app/data /app/pkg /app/pkg/default
WORKDIR /app/pkg

## Copy the code to the proper locations
## start.sh will setup and start the nodejs app
## wappler_app is the directory where the site is stored
COPY start.sh /app/pkg/
COPY node.sh /app/pkg/
COPY wappler_app /app/pkg/default

WORKDIR /app/data
## Now the Docker container will run this command when it is started.
CMD [ "/app/pkg/start.sh" ]