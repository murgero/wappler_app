#!/bin/bash

## Set script to stop if something errors
set -eu

## We can use logic here to make sure the configuration is ready, then start the application.
## This if statement checks if the wappler_app folder exists in the apps data directory.
## Will create it using a default template made from an exported site from wappler
## Will then create a tmp directory required by the site
if [[ ! -d "/app/data/wappler_app" ]]; then
    echo "=> Creating basic site for first run"
    cp -R /app/pkg/default /app/data/wappler_app
    mkdir -p /app/data/wappler_app/tmp
fi

## chown the data dir so cloudron:cloudron owns it
chown -R cloudron:cloudron /app/data

## This command is used to run the node app in the cloudron context:
exec /usr/local/bin/gosu cloudron:cloudron /app/pkg/node.sh